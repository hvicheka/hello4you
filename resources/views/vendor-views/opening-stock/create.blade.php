@extends('layouts.vendor.app')

@section('title', _translate("Opening stock"))

@push('css_or_js')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('public/assets/admin/css/tags-input.min.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title">
                        {{ _translate("Opening stock") }}
                    </h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="{{ route('vendor.opening_stock.store') }}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value="{{ $item->id }}" name="product_id">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">{{ _translate('item name') }}</label>
                                <h2>{{ $item->name }}</h2>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">{{ _translate('current_stock') }}</label>
                                <h2>{{ $item->stock ?? 0 }}</h2>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="input-label" for="store_id">{{_translate('quantity')}}<span
                                        class="input-label-secondary"></span></label>
                                <input type="number" class="form-control" name="quantity" required>
                            </div>
                        </div>
                    </div>
                    <hr>

                    <button type="submit" class="btn btn-primary">{{__('messages.submit')}}</button>

                    <div class="mt-5"></div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="hidden" id="total_amount" name="final_total" value="6">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-condensed"
                                               id="stock_adjustment_product_table">
                                            <thead>
                                            <tr>
                                                <th class="col-sm-4 text-center">Product</th>
                                                <th class="col-sm-2 text-center">Quantity</th>
                                                <th class="col-sm-2 text-center">Date</th>
                                            </tr>
                                            </thead>
                                            <tbody class="stock-table">
                                            @foreach($opening_stocks as $stock)
                                                <tr class="product_row">
                                                    <td>{{ $item->name }}</td>
                                                    <td>
                                                        @if($stock->adjust_type == 'stock_adjust')
                                                            <span class="text-danger">- {{ $stock->quantity }}</span>
                                                        @elseif($stock->adjust_type == 'opening_stock')
                                                            <span class="text-primary">+ {{ $stock->quantity }}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $stock->created_at->format('d-m-Y') }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>

@endsection


@push('script_2')
    <script src="{{asset('public/assets/admin')}}/js/tags-input.min.js"></script>
    <script>
        function getRestaurantData(route, store_id, id) {
            $.get({
                url: route + store_id,
                dataType: 'json',
                success: function (data) {
                    $('#' + id).empty().append(data.options);
                },
            });
        }

        function getRequest(route, id) {
            $.get({
                url: route,
                dataType: 'json',
                success: function (data) {
                    $('#' + id).empty().append(data.options);
                },
            });
        }
    </script>

    <script>
        var module_id = 0;
        var parent_category_id = 0;
        var module_data = null;
        var stock = true;
        var store_id = 0;


        $(document).on('ready', function () {
            // INITIALIZATION OF SELECT2
            // =======================================================
            $('.js-select2-custom').each(function () {
                var select2 = $.HSCore.components.HSSelect2.init($(this));
            });
        });

    </script>

@endpush


