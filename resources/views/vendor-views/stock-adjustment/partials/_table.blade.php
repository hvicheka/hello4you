@foreach($stocks as $key=>$stock)
    <tr>
        <td>{{$key+1}}</td>
        <td>
            {{ $stock->date }}
        </td>
        <td>
            {{ $stock->store->name }}
        </td>
        <td>{{\App\CentralLogics\Helpers::format_currency($stock['price'])}}</td>
        <td>{{($item['points'])}}</td>
        <td>
            <label class="toggle-switch toggle-switch-sm" for="stocksCheckbox{{$item->id}}">
                <input type="checkbox" onclick="location.href='{{route('admin.item.status',[$item['id'],$item->status?0:1])}}'"class="toggle-switch-input" id="stocksCheckbox{{$item->id}}" {{$item->status?'checked':''}}>
                <span class="toggle-switch-label">
                    <span class="toggle-switch-indicator"></span>
                </span>
            </label>
        </td>
        <td>
            <a class="btn btn-sm btn-white"
                href="{{route('admin.item.edit',[$item['id']])}}" title="{{__('messages.edit')}} {{__('messages.item')}}"><i class="tio-edit"></i>
            </a>
            <a class="btn btn-sm btn-white" href="javascript:"
                onclick="form_alert('food-{{$item['id']}}','{{__('messages.Want_to_delete_this_item')}}')" title="{{__('messages.delete')}} {{__('messages.item')}}"><i class="tio-delete-outlined"></i>
            </a>
            <form action="{{route('admin.item.delete',[$item['id']])}}"
                    method="post" id="food-{{$item['id']}}">
                @csrf @method('delete')
            </form>
        </td>
    </tr>
@endforeach
