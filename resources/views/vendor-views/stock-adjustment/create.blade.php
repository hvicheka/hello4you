@extends('layouts.vendor.app')

@section('title', _translate('Add Stock Adjustment'))

@push('css_or_js')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('public/assets/admin/css/tags-input.min.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title">
                        {{ _translate("Stock adjustment") }}
                    </h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="{{url('/')}}/vendor-panel/stock-adjustment" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label">{{ _translate('reference_no') }}</label>
                                <input type="text" class="form-control" name="ref_no">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="store_id">{{__('messages.date')}}</label>
                                <input type="text" class="form-control stock_adjustment_date" id="date" name="date"
                                       value="<?php echo now()->format('Y-m-d'); ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="input-label" for="reason">{{ _translate('reason') }}</label>
                                <textarea name="reason" class="form-control" id="reason" cols="30" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="product_id">{{__('messages.item')}}<span
                                        class="input-label-secondary"></span></label>
                                <select name="product_id"
                                        data-placeholder="{{__('messages.select')}} {{__('messages.item')}}"
                                        id="product_id" class="js-data-example-ajax form-control"
                                        oninvalid="this.setCustomValidity('{{__('messages.please_select_store')}}')">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="hidden" id="total_amount" name="final_total" value="6">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-condensed"
                                               id="stock_adjustment_product_table">
                                            <thead>
                                            <tr>
                                                <th class="col-sm-4 text-center">Product</th>
                                                <th class="col-sm-2 text-center">Quantity</th>
                                                <th class="col-sm-2 text-center">Unit Price</th>
                                                <th class="col-sm-2 text-center">Subtotal</th>
                                                <th class="col-sm-2 text-center"><i class="fa fa-trash"
                                                                                    aria-hidden="true"></i></th>
                                            </tr>
                                            </thead>
                                            <tbody class="stock-table">
                                            </tbody>
                                            <tfoot>
                                            <tr class="text-center">
                                                <td colspan="3"></td>
                                                <td>
                                                    <div class="pull-right"><b>{{ _translate('Total Amount') }}:</b>
                                                        <span id="total_adjustment">00.00</span></div>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-primary">{{__('messages.submit')}}</button>
                </form>
            </div>
        </div>
    </div>

@endsection


@push('script_2')
    <script src="{{asset('public/assets/admin')}}/js/tags-input.min.js"></script>
    <script>
        var store_id = "{{ $store_id }}";

        function getRestaurantData(route, store_id, id) {
            $.get({
                url: route + store_id,
                dataType: 'json',
                success: function (data) {
                    $('#' + id).empty().append(data.options);
                },
            });
        }

        function getRequest(route, id) {
            $.get({
                url: route,
                dataType: 'json',
                success: function (data) {
                    $('#' + id).empty().append(data.options);
                },
            });
        }
    </script>

    <script>


        $(document).on('ready', function () {
            // INITIALIZATION OF SELECT2
            // =======================================================
            $('.js-select2-custom').each(function () {
                var select2 = $.HSCore.components.HSSelect2.init($(this));
            });
        });


        $('#product_id').select2({
            allowClear: true,
            ajax: {
                url: '{{url('/')}}/vendor-panel/stock-adjustment/get-products',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    return {
                        q: params.term,
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
            },

        }).on('select2:select', function () {
            const product_id = $('#product_id').val()
            get_product_detail(product_id)
        });

        function get_product_detail(product_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('/')}}/vendor-panel/stock-adjustment/get-products-detail",
                method: 'GET',
                data: {
                    item_id: product_id
                },
                success: function (result) {
                    let item_price = result.price.toFixed(2)
                    let item_name = result.name
                    var rows = ($('#stock_adjustment_product_table >tbody >tr').length) + 1;
                    let row = `
                        <tr class="product_row">
                             <td>${item_name}</td>
                             <td>
                                  <input type="hidden" name="products[${rows}][product_id]" class="form-control product_id" value="${result.id}">
                                  <input type="hidden" name="products[${rows}][store_id]" class="form-control product_id" value="${store_id}">
                                  <input type="number" class="form-control product_quantity input_number input_quantity valid" value="1" name="products[${rows}][quantity]" data-rule-abs_digit="true" data-msg-abs_digit="Decimal value not allowed" data-decimal="0" data-rule-required="true" data-msg-required="This field is required" data-rule-max-value="993.0000" data-msg-max-value="Only 993.00 Pc(s) available" data-qty_available="993.0000" data-msg_max_default="Only 993.00 Pc(s) available" aria-required="true">
                             </td>
                             <td>
                                 <input type="text" readonly name="products[${rows}][unit_price]" class="form-control product_unit_price input_number" value="${item_price}">
                             </td>
                             <td>
                                 <input type="text" readonly="" name="products[${rows}][price]" class="form-control product_line_total valid" value="${item_price}" aria-invalid="false">
                             </td>
                             <td class="text-center">
                                 <button class="remove_product_row btn btn-sm btn-danger" data-id=""><i class="tio-delete-outlined  cursor-pointer" aria-hidden="true"></i></button>
                             </td>
                        </tr>
                    `
                    $('.stock-table').append(row)
                    calculateSum()
                }
            });
        }

    </script>

    <script>
        $(function () {
            $(document).on('keyup', '.input_quantity', function () {
                console.log('calculate')
                calculateSum();
            });
            $(document).on('click', '.remove_product_row', function () {
                $(this).parents('.product_row').remove()
                calculateSum()
            })
        });

        function calculateSum() {
            var sum = 0.0;
            $('#stock_adjustment_product_table > tbody  > tr').each(function () {
                var qty = $(this).find('.input_quantity').val();
                var price = $(this).find('.product_unit_price').val();
                var amount = parseFloat(qty) * parseFloat(price);
                amount = isNaN(amount) ? 0 : amount;  //checks for initial empty text boxes
                sum += amount;
                $(this).find('.product_line_total').text('' + amount);
                $(this).find('.product_line_total').val('' + amount);  // for inputs/textbox
            });
            //just update the total to sum
            $('#total_adjustment').text(sum);
            $('#total_amount').val(sum);  // for inputs/textbox
        }
    </script>

@endpush


