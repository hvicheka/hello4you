@foreach($brands as $key=>$brand)
<tr>
    <td>{{$key+1}}</td>
    <td>{{$brand->id}}</td>
    <td>
        <span class="d-block font-size-sm text-body">
            {{Str::limit($brand['name'], 20,'...')}}
        </span>
    </td>
    <td>
        <span class="d-block font-size-sm text-body">
            {{Str::limit($brand->module->module_name, 15,'...')}}
        </span>
    </td>
    <td>
        <label class="toggle-switch toggle-switch-sm" for="stocksCheckbox{{$brand->id}}">
        <input type="checkbox" onclick="location.href='{{route('admin.brand.status',[$brand['id'],$brand->status?0:1])}}'"class="toggle-switch-input" id="stocksCheckbox{{$brand->id}}" {{$brand->status?'checked':''}}>
            <span class="toggle-switch-label">
                <span class="toggle-switch-indicator"></span>
            </span>
        </label>
    </td>
    <td>
        <form action="{{route('admin.brand.priority',$brand->id)}}">
        <select name="priority" id="priority" class="w-100" onchange="this.form.submit()">
            <option value="0" {{$brand->priority == 0?'selected':''}}>{{__('messages.normal')}}</option>
            <option value="1" {{$brand->priority == 1?'selected':''}}>{{__('messages.medium')}}</option>
            <option value="2" {{$brand->priority == 2?'selected':''}}>{{__('messages.high')}}</option>
        </select>
        </form>
    </td>
    <td>
        <a class="btn btn-sm btn-white"
            href="{{route('admin.brand.edit',[$brand['id']])}}" title="{{__('messages.edit')}} {{__('messages.brand')}}"><i class="tio-edit"></i>
        </a>
        <a class="btn btn-sm btn-white" href="javascript:"
        onclick="form_alert('brand-{{$brand['id']}}','Want to delete this brand')" title="{{__('messages.delete')}} {{__('messages.brand')}}"><i class="tio-delete-outlined"></i>
        </a>
        <form action="{{route('admin.brand.delete',[$brand['id']])}}" method="post" id="brand-{{$brand['id']}}">
            @csrf @method('delete')
        </form>
    </td>
</tr>
@endforeach
