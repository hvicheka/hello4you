@extends('layouts.admin.app')

@section('title','Add new sub sub category')

@push('css_or_js')

@endpush

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title">Sub Sub Category</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form
                    action="{{isset($category)?route('admin.category.update',[$category['id']]):route('admin.category.store')}}"
                    method="post">
                    @csrf
                    @php($language=\App\Models\BusinessSetting::where('key','language')->first())
                    @php($language = $language->value ?? null)
                    @php($default_lang = 'en')
                    @if($language)
                        @php($default_lang = json_decode($language)[0])
                        <ul class="nav nav-tabs mb-4">
                            @foreach(json_decode($language) as $lang)
                                <li class="nav-item">
                                    <a class="nav-link lang_link {{$lang == $default_lang? 'active':''}}" href="#"
                                       id="{{$lang}}-link">{{\App\CentralLogics\Helpers::get_language_name($lang).'('.strtoupper($lang).')'}}</a>
                                </li>
                            @endforeach
                        </ul>
                        @foreach(json_decode($language) as $lang)
                            <div class="form-group {{$lang != $default_lang ? 'd-none':''}} lang_form"
                                 id="{{$lang}}-form">
                                <label class="input-label" for="exampleFormControlInput1">{{__('messages.name')}}
                                    ({{strtoupper($lang)}})</label>
                                <input type="text" name="name[]" class="form-control"
                                       placeholder="{{__('messages.new_category')}}" maxlength="191"
                                       {{$lang == $default_lang? 'required':''}} oninvalid="document.getElementById('en-link').click()">
                            </div>
                            <input type="hidden" name="lang[]" value="{{$lang}}">
                        @endforeach
                    @else
                        <div class="form-group">
                            <label class="input-label" for="exampleFormControlInput1">{{__('messages.name')}}</label>
                            <input type="text" name="name" class="form-control"
                                   placeholder="{{__('messages.new_category')}}" value="{{old('name')}}" required
                                   maxlength="191">
                        </div>
                        <input type="hidden" name="lang[]" value="{{$lang}}">
                    @endif
                    <div class="form-group">
                        <label class="input-label"
                               for="main_category">{{__('messages.main')}} {{__('messages.category')}}
                            <span class="input-label-secondary">*</span></label>
                        <select id="main_category" name="main_category" class="form-control js-select2-custom"
                                required>
                            @foreach(\App\Models\Category::where(['position'=>0])->get() as $cat)
                                <option
                                    value="{{$cat['id']}}" {{isset($category)?($category['parent_id']==$cat['id']?'selected':''):''}} >{{$cat['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="input-label"
                               for="parent_id">{{__('messages.sub')}} {{__('messages.category')}}
                        </label>
                        <select id="parent_id" name="parent_id" class="form-control js-select2-custom"
                                required>
                        </select>
                    </div>
                    <input name="position" value="2" style="display: none">
                    <button type="submit"
                            class="btn btn-primary">{{isset($category)?__('messages.update'):__('messages.add')}}</button>
                </form>
            </div>

            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <hr>
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-title"></h5>
                    </div>
                    <!-- Table -->
                    <div class="table-responsive datatable-custom">
                        <table id="columnSearchDatatable"
                               class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table"
                               data-hs-datatables-options='{
                                 "order": [],
                                 "orderCellsTop": true
                               }'>
                            <thead class="thead-light">
                            <tr>
                                <th>#sl</th>
                                <th style="width: 50%">Main Category</th>
                                <th style="width: 50%">Sub Category</th>
                                <th style="width: 50%">Sub Sub Category</th>
                                <th style="width: 20%">Status</th>
                                <th style="width: 10%">Action</th>
                            </tr>
                            {{--                            <tr>--}}
                            {{--                                <th></th>--}}
                            {{--                                <th>--}}
                            {{--                                    <input type="text" id="column1_search" class="form-control form-control-sm"--}}
                            {{--                                           placeholder="Search Sub Category">--}}
                            {{--                                </th>--}}

                            {{--                                <th>--}}
                            {{--                                    <input type="text" id="column2_search" class="form-control form-control-sm"--}}
                            {{--                                           placeholder="Search Sub Sub Category">--}}
                            {{--                                </th>--}}

                            {{--                                <th>--}}
                            {{--                                    <select id="column3_search" class="js-select2-custom"--}}
                            {{--                                            data-hs-select2-options='{--}}
                            {{--                                              "minimumResultsForSearch": "Infinity",--}}
                            {{--                                              "customClass": "custom-select custom-select-sm text-capitalize"--}}
                            {{--                                            }'>--}}
                            {{--                                        <option value="">Any</option>--}}
                            {{--                                        <option value="Active">Active</option>--}}
                            {{--                                        <option value="Disabled">Disabled</option>--}}
                            {{--                                    </select>--}}
                            {{--                                </th>--}}
                            {{--                                <th>--}}
                            {{--                                    --}}{{--<input type="text" id="column4_search" class="form-control form-control-sm"--}}
                            {{--                                           placeholder="Search countries">--}}
                            {{--                                </th>--}}
                            {{--                            </tr>--}}
                            </thead>

                            <tbody>
                            @foreach(\App\Models\Category::with(['parent'])->where(['position'=>2])->latest()->get() as $key=>$category)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>
                                        <span class="d-block font-size-sm text-body">
                                            {{$category->parent->parent->name ?? ''}}
                                        </span>
                                    </td>
                                    <td>
                                        <span class="d-block font-size-sm text-body">
                                            {{$category->parent_id!=0?$category->parent['name']:''}}
                                        </span>
                                    </td>

                                    <td>
                                        <span class="d-block font-size-sm text-body">
                                            {{$category['name']}}
                                        </span>
                                    </td>

                                    <td>
                                        <label class="toggle-switch toggle-switch-sm"
                                               for="stocksCheckbox{{$category->id}}">
                                            <input type="checkbox"
                                                   onclick="location.href='{{route('admin.category.status',[$category['id'],$category->status?0:1])}}'"
                                                   class="toggle-switch-input"
                                                   id="stocksCheckbox{{$category->id}}" {{$category->status?'checked':''}}>
                                            <span class="toggle-switch-label">
                                            <span class="toggle-switch-indicator"></span>
                                        </span>
                                        </label>
                                    </td>
                                    <td>
                                        <!-- Dropdown -->
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                <i class="tio-settings"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item"
                                                   href="{{route('admin.category.edit',[$category['id']])}}">Edit</a>
                                                <a class="dropdown-item" href="javascript:"
                                                   onclick="$('#category-{{$category['id']}}').submit()">Delete</a>
                                                <form action="{{route('admin.category.delete',[$category['id']])}}"
                                                      method="post" id="category-{{$category['id']}}">
                                                    @csrf @method('delete')
                                                </form>
                                            </div>
                                        </div>
                                        <!-- End Dropdown -->
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End Table -->
        </div>
    </div>

@endsection

@push('script_2')
    <script>
        $(document).on('ready', function () {
            // INITIALIZATION OF DATATABLES
            // =======================================================
            var datatable = $.HSCore.components.HSDatatables.init($('#columnSearchDatatable'));

            $('#column1_search').on('keyup', function () {
                datatable
                    .columns(1)
                    .search(this.value)
                    .draw();
            });


            $('#column2_search').on('keyup', function () {
                datatable
                    .columns(2)
                    .search(this.value)
                    .draw();
            });

            $('#column3_search').on('change', function () {
                datatable
                    .columns(3)
                    .search(this.value)
                    .draw();
            });

            // INITIALIZATION OF SELECT2
            // =======================================================
            $('.js-select2-custom').each(function () {
                var select2 = $.HSCore.components.HSSelect2.init($(this));
            });
        });
    </script>



    <script>

        $('#main_category').on('change', function () {
            var main_category_id = this.value;
            $("#parent_id").html('');
            $.ajax({
                url: "{{ route('admin.ajax.get_sub_category') }}",
                type: "GET",
                data: {
                    parent_id: main_category_id,
                },
                dataType: 'json',
                success: function (result) {
                    if (result.length > 0) {
                        $('#parent_id').html(`<option value="">-- Select Sub Category --</option>`);
                        $.each(result, function (key, value) {
                            $("#parent_id").append(`<option value="${value.id}">${value.name}</option>`);
                        });
                    } else {
                        $('#parent_id').html(``);
                    }
                }
            });
        });

    </script>
@endpush
