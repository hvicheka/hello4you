@extends('layouts.admin.app')

@section('title','Stock Adjustment Preview')

@push('css_or_js')

@endpush

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">

            </div>
        </div>
        <!-- End Page Header -->

        <!-- Card -->
        <div class="card mb-3 mb-lg-5">
            <!-- Body -->
            <div class="card-body">
                <div class="row align-items-md-center gx-md-5">

                    <div class="col-sm-4 mb-3 mb-md-0">
                        <div class="d-flex align-items-center">


                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-6 col-12 pt-2">
                        <h4>{{ _translate('date') }} : </h4>
                        {{ $stock->date->format('d-m-Y')}}
                    </div>
                    <div class="col-sm-6 col-12 pt-2 border-left">
                        <h4>{{ _translate('ref_no') }} : </h4>
                        {{ $stock->ref_no }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-12 pt-2">
                        <h4>{{ _translate('store') }} : </h4>
                        {{ $stock->store->name ?? 'Unknown'}}
                    </div>
                    <div class="col-sm-6 col-12 pt-2 border-left">
                        <h4>{{ _translate('price') }} : </h4>
                        {{\App\CentralLogics\Helpers::format_currency($stock->total)}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-12 pt-2">
                        <h4>{{ _translate('reason') }} : </h4>
                        {{ $stock->reason }}
                    </div>
                </div>
            </div>
            <!-- End Body -->
        </div>
        <!-- End Card -->

        <!-- Card -->
        <div class="card">
            <div class="card-header">
                {{__('messages.product')}}
            </div>
            <!-- Table -->
            <div class="table-responsive datatable-custom">
                <table id="datatable" class="table table-borderless table-thead-bordered table-nowrap card-table"
                       data-hs-datatables-options='{
                     "columnDefs": [{
                        "targets": [0, 3, 6],
                        "orderable": false
                      }],
                     "order": [],
                     "info": {
                       "totalQty": "#datatableWithPaginationInfoTotalQty"
                     },
                     "search": "#datatableSearch",
                     "entries": "#datatableEntries",
                     "pageLength": 25,
                     "isResponsive": false,
                     "isShowPaging": false,
                     "pagination": "datatablePagination"
                   }'>
                    <thead class="thead-light">
                    <tr>
                        <th>{{_translate('ID')}}</th>
                        <th>{{_translate('item')}}</th>
                        <th>{{_translate('quantity')}}</th>
                        <th>{{_translate('unit_price')}}</th>
                        <th>{{_translate('price')}}</th>
                        <th>{{_translate('sub_total')}}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($stock_products as $product)
                        <tr>
                            <td>{{ ++$loop->index }}</td>
                            <td>{{ $product->item->name }}</td>
                            <td>{{ $product->quantity }}</td>
                            <td> {{\App\CentralLogics\Helpers::format_currency($product->unit_price)}}</td>
                            <td> {{\App\CentralLogics\Helpers::format_currency($product->price)}} </td>
                            <td> {{\App\CentralLogics\Helpers::format_currency($product->price * $product->unit_price)}} </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- End Table -->

            <!-- Footer -->
            <div class="card-footer">
                <!-- Pagination -->
                <div class="row justify-content-center justify-content-sm-between align-items-sm-center">
                    <div class="col-12">

                    </div>
                </div>
                <!-- End Pagination -->
            </div>
            <!-- End Footer -->
        </div>
        <!-- End Card -->
    </div>
@endsection

@push('script_2')
    <script>
        function status_form_alert(id, message, e) {
            e.preventDefault();
            Swal.fire({
                title: '{{__('messages.are_you_sure')}}',
                text: message,
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: 'default',
                confirmButtonColor: '#FC6A57',
                cancelButtonText: 'No',
                confirmButtonText: 'Yes',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $('#' + id).submit()
                }
            })
        }
    </script>
@endpush
