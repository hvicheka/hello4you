@extends('layouts.admin.app')

@section('title','Stock Adjustment List')

@push('css_or_js')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm-4 col-12">
                    <h1 class="page-header-title"> {{_translate('Stock adjustment list')}}<span
                            class="badge badge-soft-dark ml-2" id="foodCount">{{$stocks->total()}}</span></h1>
                </div>

                <div class="col-sm-4 col-6">
                    <form action="{{ url('/admin/stock-adjustment') }}" id="stock_filter_store" method="GET">
                        <select name="store_id" id="select_store" class="form-control js-select2-custom">
                            <option
                                value="" {{!request('module_id') ? 'selected':''}}>{{ _translate('all stores') }}</option>
                            @foreach (\App\Models\Store::all() as $store)
                                <option
                                    value="{{$store->id}}" {{request('store_id') == $store->id?'selected':''}}>
                                    {{$store->name ?? 'Unknown' }}
                                </option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>

        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <!-- Card -->
                <div class="card">
                    <!-- Header -->
                    <div class="card-header p-1">
                        <div class="row justify-content-between align-items-center flex-grow-1">
                            <div class="col-md-4 mb-3 mb-md-0">

                            </div>

                            <div class="col-auto">
                            </div>
                        </div>
                        <!-- End Row -->
                    </div>
                    <!-- End Header -->

                    <!-- Table -->
                    <div class="table-responsive datatable-custom" id="table-div">
                        <table id="datatable"
                               class="table table-borderless table-thead-bordered table-nowrap table-align-middle card-table"
                               data-hs-datatables-options='{
                                "columnDefs": [{
                                    "targets": [],
                                    "width": "5%",
                                    "orderable": false
                                }],
                                "order": [],
                                "info": {
                                "totalQty": "#datatableWithPaginationInfoTotalQty"
                                },

                                "entries": "#datatableEntries",

                                "isResponsive": false,
                                "isShowPaging": false,
                                "paging":false
                            }'>
                            <thead class="thead-light">
                            <tr>
                                <th>{{__('messages.#')}}</th>
                                <th style="width: 20%">{{translate('date')}}</th>
                                <th style="width: 20%">{{_translate('ref_no')}}</th>
                                <th style="width: 15%">{{__('messages.store')}}</th>
                                <th>{{__('messages.price')}}</th>
                                <th style="width: 15%">{{translate('reason')}}</th>
                                <th>{{__('messages.action')}}</th>
                            </tr>
                            </thead>

                            <tbody id="set-rows">
                            @foreach($stocks as $key=>$stock)
                                <tr>
                                    <td>{{ ++$loop->index }}</td>
                                    <td>
                                        {{ $stock->date->format('m-d-Y') }}
                                    </td>
                                    <td>
                                        {{ $stock->ref_no }}
                                    </td>
                                    <td>
                                        {{Str::limit($stock->store?$stock->store->name:__('messages.store deleted!'), 20, '...')}}
                                    </td>
                                    <td>{{\App\CentralLogics\Helpers::format_currency($stock['total'])}}</td>
                                    <td>{{Str::limit($stock->reason, 20, '...')}}</td>
                                    <td>
                                        <a class="btn btn-sm btn-white"
                                           href="{{route('admin.stock_adjustment.show', $stock->id)}}"
                                           title="{{__('messages.view')}}"><i
                                                class="tio-visible"></i>
                                        </a>
                                        <a class="btn btn-sm btn-white" href="javascript:"
                                           onclick="form_alert('food-{{$stock['id']}}','{{__('messages.Want_to_delete_this_item')}}')"
                                           title="{{__('messages.delete')}} {{__('messages.item')}}"><i
                                                class="tio-delete-outlined"></i>
                                        </a>

                                        <form action="{{route('admin.stock_adjustment.delete', $stock->id)}}"
                                              method="post" id="food-{{$stock['id']}}">
                                            @csrf @method('delete')
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <div class="page-area">
                            <table>
                                <tfoot class="border-top">
                                {!! $stocks->withQueryString()->links() !!}
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- End Table -->
                </div>
                <!-- End Card -->
            </div>
        </div>
    </div>

@endsection

@push('script_2')
    <script>
        $(document).on('ready', function () {
            // INITIALIZATION OF DATATABLES
            // =======================================================
            var datatable = $.HSCore.components.HSDatatables.init($('#datatable'), {
                select: {
                    style: 'multi',
                    classMap: {
                        checkAll: '#datatableCheckAll',
                        counter: '#datatableCounter',
                        counterInfo: '#datatableCounterInfo'
                    }
                },
                language: {
                    zeroRecords: '<div class="text-center p-4">' +
                        '<img class="mb-3" src="{{asset('public/assets/admin/svg/illustrations/sorry.svg')}}" alt="Image Description" style="width: 7rem;">' +
                        '<p class="mb-0">No data to show</p>' +
                        '</div>'
                }
            });

            // INITIALIZATION OF SELECT2
            // =======================================================
            $('.js-select2-custom').each(function () {
                var select2 = $.HSCore.components.HSSelect2.init($(this));
            });
        });

        $('#select_store').on('change', function () {
            console.log('change')
            $('#stock_filter_store').submit();
        });

    </script>
@endpush
