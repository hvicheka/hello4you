<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\StockAdjustmentProduct;
use Illuminate\Http\Request;

class OpeningStockController extends Controller
{

    public function create($id)
    {
        $item = Item::query()
            ->findOrFail($id);

        $opening_stocks = StockAdjustmentProduct::query()
            ->where('adjust_type', 'opening_stock')
            ->where('item_id', $id)
            ->get();
        return view('vendor-views.opening-stock.create', compact('item', 'opening_stocks'));
    }

    public function store(Request $request)
    {
        $item = Item::query()
            ->findOrFail($request->product_id);
        $adjust = StockAdjustmentProduct::create([
            'adjust_type' => "opening_stock",
            'previous_quantity' => $item->stock ?? 0,
            'item_id' => $request->product_id,
            'quantity' => $request->quantity
        ]);

        $item->stock = $item->stock + $adjust->quantity;
        $item->save();
        return redirect()->back();
    }

}
