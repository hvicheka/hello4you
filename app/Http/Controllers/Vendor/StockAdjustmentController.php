<?php

namespace App\Http\Controllers\Vendor;

use App\CentralLogics\Helpers;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\StockAdjustment;
use App\Models\StockAdjustmentProduct;
use App\Models\Vendor;
use App\Models\VendorEmployee;
use App\Scopes\StoreScope;
use Illuminate\Http\Request;

class StockAdjustmentController extends Controller
{
    public function index()
    {
        $stocks = StockAdjustment::query()
            ->latest()
            ->where('store_id', auth('vendor')->id())
            ->paginate(config('default_pagination'));
        return view('vendor-views.stock-adjustment.index', compact('stocks'));
    }

    public function create()
    {
        $store_id = Helpers::get_store_id();
        return view('vendor-views.stock-adjustment.create', compact('store_id'));
    }

    public function store(Request $request)
    {

        $stock_adjustment_product = [];

        $store_id = Helpers::get_store_id();

        $data = [
            'store_id' => $store_id,
            'ref_no' => $request->ref_no,
            'total' => $request->final_total,
            'date' => $request->date,
            'reason' => $request->reason,
        ];

        if (auth('vendor_employee')->check()) {
            $data['userable_id'] = auth('vendor_employee')->id();
            $data['userable_type'] = VendorEmployee::class;
        } else {
            $data['userable_id'] = auth('vendor')->id();
            $data['userable_type'] = Vendor::class;
        }


        $stock_adjustment = StockAdjustment::create($data);

        foreach ($request->products as $product) {
            $stock_adjustment_product[] = $product;
        }

        foreach ($stock_adjustment_product as $product) {
            $item = Item::query()->find($product['product_id']);
            if ($item) {
                StockAdjustmentProduct::create([
                    'adjust_type' => "stock_adjust",
                    'previous_quantity' => $item->stock ?? 0,
                    'stock_adjustment_id' => $stock_adjustment->id,
                    'item_id' => $product['product_id'],
                    'quantity' => $product['quantity'],
                    'unit_price' => $product['unit_price'],
                    'price' => $product['price']
                ]);
                $item->decrement('stock', $product['quantity']);
            }
        }

        toastr(_translate('Stock adjustment added successfully'));
        return redirect(url('/vendor-panel/stock-adjustment'));
    }

    public function show($id)
    {
        $stock = StockAdjustment::query()->findOrFail($id);
        $stock_products = StockAdjustmentProduct::query()
            ->where('stock_adjustment_id', $id)
            ->where( 'adjust_type',"stock_adjust")
            ->get();
        return view('vendor-views.stock-adjustment.show', compact('stock', 'stock_products'));
    }

    public function get_products(Request $request)
    {
        $items = Item::withoutGlobalScope(StoreScope::class)
            ->select('id', 'name')
            ->where('store_id', auth('vendor')->id())
            ->when($request->q, function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->q . '%');
            })
            ->when($request->module_id, function ($q) use ($request) {
                $q->where('module_id', $request->module_id);
            })
            ->get();
        $results = [];
        foreach ($items as $item) {
            $results[] = [
                'id' => $item->id,
                'text' => $item->name
            ];
        }
        return response()->json($results);
    }

    public function get_products_detail(Request $request)
    {
        $item = Item::query()
            ->select('id', 'name', 'price')
            ->where('id', $request->item_id)
            ->first();
        return response()->json($item);
    }


    public function destroy($id)
    {
        $stock = StockAdjustment::query()->findOrFail($id);
        $stock_products = StockAdjustmentProduct::query()
            ->where('stock_adjustment_id', $stock->id)
            ->where( 'adjust_type',"stock_adjust")
            ->get();
        $product_stock_ids = [];
        foreach ($stock_products as $product) {
            $item = Item::find($product->item_id);
            $item->increment('stock', $product->quantity);
            $product_stock_ids[] = $product->id;
        }

        StockAdjustmentProduct::destroy($product_stock_ids);
        $stock->delete();
        return redirect(url('/vendor-panel/stock-adjustment'));
    }
}
