<?php

namespace App\Http\Controllers\Vendor;

use App\CentralLogics\Helpers;
use App\Http\Controllers\Controller;
use App\Models\StockAdjustmentProduct;
use App\Models\User;
use App\Models\Zone;
use App\Models\Store;
use Illuminate\Http\Request;
use App\Scopes\StoreScope;

class ReportController extends Controller
{

    public function stock_report(Request $request)
    {
        $key = explode(' ', $request['search']);


        $zone_id = $request->query('zone_id', isset(auth('admin')->user()->zone_id) ? auth('admin')->user()->zone_id : 'all');
        $store_id = $request->query('store_id', 'all');
        $zone = is_numeric($zone_id) ? Zone::findOrFail($zone_id) : null;
        $store = is_numeric($store_id) ? Store::findOrFail($store_id) : null;
        $items = \App\Models\Item::query()
            ->withCount(['stock_adjusts'])
            ->where(function ($q) use ($key) {
                foreach ($key as $value) {
                    $q->orWhere('name', 'like', "%{$value}%");
                }
            })
            ->paginate(25);
        return view('vendor-views.report.stock-report', compact('zone', 'store', 'items'));

    }

    public function stock_report_history($id)
    {
        $item = \App\Models\Item::query()
            ->withoutGlobalScope(StoreScope::class)
            ->findOrFail($id);
        $stock_products = StockAdjustmentProduct::query()
            ->where('item_id', $id)
            ->get();
        return view('vendor-views.report.stock-report-history', compact('stock_products', 'item'));

    }

}
