<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Item;
use App\Models\User;
use App\Models\Zone;
use App\Models\Order;
use App\Models\Point;
use App\Models\Points;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\CentralLogics\Helpers;

use App\Models\BusinessSetting;
use App\Models\CustomerAddress;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function address_list(Request $request)
    {
        return response()->json(CustomerAddress::where('user_id', $request->user()->id)->latest()->get(), 200);
    }

    public function points_list(Request $request)
    {
        $points = Point::where('customer_id', $request->user()->id)
            ->where('points', '>', 0)
            ->orWhere('points', '<', 0)
            ->latest()
            ->paginate($request->limit, ['*'], 'page', $request->offset);
        return response()->json([
            'total_size' => $points->total(),
            'limit' => $request->limit,
            'offset' => $request->offset,
            'points' => $points->items()
        ], 200);
    }

    public function add_new_address(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'contact_person_name' => 'required',
            'address_type' => 'required',
            'contact_person_number' => 'required',
            'address' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

//        $point = new Point($request->latitude, $request->longitude);
//        $zone = Zone::active()->contains('coordinates', $point)->latest()->first();
//        if (!$zone) {
//            $errors = ['code' => 'coordinates', 'message' => trans('messages.service_not_available_in_this_area')];
//            return response()->json([
//                'errors' => $errors
//            ], 403);
//        }

        $address = [
            'user_id' => $request->user()->id,
            'contact_person_name' => $request->contact_person_name,
            'contact_person_number' => $request->contact_person_number,
            'address_type' => $request->address_type,
            'address' => $request->address,
            'longitude' => $request->longitude,
            'latitude' => $request->latitude,
//            'zone_id' => $zone->id,
            'created_at' => now(),
            'updated_at' => now()
        ];
        DB::table('customer_addresses')->insert($address);
        return response()->json(['message' => trans('messages.successfully_added')], 200);
    }

    public function update_address(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'contact_person_name' => 'required',
            'address_type' => 'required',
            'contact_person_number' => 'required',
            'address' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }
//        $point = new Point($request->latitude, $request->longitude);
//        $zone = Zone::active()->contains('coordinates', $point)->first();
//        if (!$zone) {
//            $errors = ['code' => 'coordinates', 'message' => trans('messages.service_not_available_in_this_area')];
//            return response()->json([
//                'errors' => $errors
//            ], 403);
//        }
        $address = [
            'user_id' => $request->user()->id,
            'contact_person_name' => $request->contact_person_name,
            'contact_person_number' => $request->contact_person_number,
            'address_type' => $request->address_type,
            'address' => $request->address,
            'longitude' => $request->longitude,
            'latitude' => $request->latitude,
//            'zone_id' => $zone->id,
            'created_at' => now(),
            'updated_at' => now()
        ];
        DB::table('customer_addresses')->where('id', $id)->update($address);
        return response()->json(['message' => trans('messages.updated_successfully')], 200);
    }

    public function delete_address(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        if (DB::table('customer_addresses')->where(['id' => $request['address_id'], 'user_id' => $request->user()->id])->first()) {
            DB::table('customer_addresses')->where(['id' => $request['address_id'], 'user_id' => $request->user()->id])->delete();
            return response()->json(['message' => trans('messages.successfully_removed')], 200);
        }
        return response()->json(['message' => trans('messages.not_found')], 404);
    }

    public function get_order_list(Request $request)
    {
        $orders = Order::where(['user_id' => $request->user()->id])->get();
        return response()->json($orders, 200);
    }

    public function get_order_details(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $details = OrderDetail::where(['order_id' => $request['order_id']])->get();
        foreach ($details as $det) {
            $det['product_details'] = json_decode($det['product_details'], true);
        }

        return response()->json($details, 200);
    }

    public function info(Request $request)
    {

        $data = User::query()
            ->WithShares()
            ->findOrFail($request->user()->id);
        $data['total_points'] = number_format($data['total_points'] ?? 0, 2);
        $data['total_points_to_dollar'] = number_format(($data['point_to_usd'] ?? 0), 2);
        $data['order_count'] = (integer)$request->user()->orders->count();
        $data['member_since_days'] = (integer)$request->user()->created_at->diffInDays();
        unset($data['orders']);
        unset($data['point_to_usd']);
        return response()->json($data, 200);
    }

    public function update_password(Request $request)
    {
        try {
            $user = $request->user();
            $update = $user->update(['password' => Hash::make($request->password)]);
            if($update){
                return response()->json(['message' => __('Update successfully')], 200);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'errors' => null,
                'message' => $th->getMessage(),
                ], 200);
        }
        return response()->json([
            'errors' => null,
            'message' => __('Update unsuccessfully'),
            ], 200);
    }
    public function update_profile(Request $request)
    {
        if ($request['password'] != null) {
            return $this->update_password($request);
        }

        $user_id = $request->user()->id;
        $rules = [
            'f_name' => 'required',
            'l_name' => 'required',
        ];

        if($request->email != $request->user()->email){
            $rules['email'] ='required|unique:users,email,' . $user_id ;
        }
        if($request->phone != $request->user()->phone){
            $rules['phone'] ='required|unique:users,phone,' . $user_id ;
        }


        $validator = Validator::make($request->all(), $rules, [
            'f_name.required' => 'First name is required!',
            'l_name.required' => 'Last name is required!',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }


        if ($request->has('image')) {
            $imageName = Helpers::update('profile/', $request->user()->image, 'png', $request->file('image'));
        } else {
            $imageName = $request->user()->image;
        }

        if ($request['password'] != null && strlen($request['password']) > 5) {
            $pass = bcrypt($request['password']);
        } else {
            $pass = $request->user()->password;
        }

        $phone = format_phone($request->phone);

        $userDetails = [
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'image' => $imageName,
            'phone' => $phone['phone'],
            'country_code' => $phone['country_code'],
            'national_number' => $phone['national_number'],
            'password' => $pass,
        ];


        $user = User::where(['id' => $user_id]);
        $user->update($userDetails);

        return response()->json(['message' => trans('messages.successfully_updated')], 200);
    }

    public function update_interest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'interest' => 'required|array',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $userDetails = [
            'interest' => json_encode($request->interest),
        ];

        User::where(['id' => $request->user()->id])->update($userDetails);

        return response()->json(['message' => trans('messages.interest_updated_successfully')], 200);
    }

    public function update_cm_firebase_token(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cm_firebase_token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        DB::table('users')->where('id', $request->user()->id)->update([
            'cm_firebase_token' => $request['cm_firebase_token']
        ]);

        return response()->json(['message' => trans('messages.updated_successfully')], 200);
    }

    public function get_suggested_item(Request $request)
    {
        if (!$request->hasHeader('zoneId')) {
            $errors = ['code' => 'zoneId', 'message' => 'Zone id is required!'];
            return response()->json([
                'errors' => $errors
            ], 403);
        }


        $zone_id = $request->header('zoneId');

        $interest = $request->user()->interest;
        $interest = isset($interest) ? json_decode($interest) : null;
        // return response()->json($interest, 200);

        $products = Item::active()->whereHas('store', function ($q) use ($zone_id) {
            $q->where('zone_id', $zone_id);
        })
            ->when(isset($interest), function ($q) use ($interest) {
                return $q->whereIn('category_id', $interest);
            })
            ->when($interest == null, function ($q) {
                return $q->popular();
            })->limit(5)->get();
        $products = Helpers::product_data_formatting($products, true, false, app()->getLocale());
        return response()->json($products, 200);
    }

    public function update_zone(Request $request)
    {
        if (!$request->hasHeader('zoneId') && is_numeric($request->header('zoneId'))) {
            $errors = [];
            $errors[] = ['code' => 'zoneId', 'message' => trans('messages.zone_id_required')];
            return response()->json([
                'errors' => $errors
            ], 403);
        }

        $customer = $request->user();
        $customer->zone_id = (integer)$request->header('zoneId');
        $customer->save();
        return response()->json([], 200);
    }

    public function delete_account(Request $request)
    {
        $user = User::query()->findOrFail($request->user()->id);
        $user->delete();
        return response()->json([
            'message' => _translate('Account deleted successfully')
        ]);
    }
}
