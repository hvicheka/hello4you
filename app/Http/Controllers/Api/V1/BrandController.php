<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Item;
use App\CentralLogics\Helpers;
use App\Http\Controllers\Controller;
use App\Models\Brand;

class BrandController extends Controller
{
    public function get_brands()
    {
        try {
            $brands = Brand::when(config('module.current_module_id'), function ($query) {
                $query->module(config('module.current_module_id'));
            })
                ->orderBy('priority', 'desc')->get();
            return response()->json(Helpers::brand_data_formatting($brands, true), 200);
        } catch (\Exception $e) {
            return response()->json([], 200);
        }
    }

    public function get_store_brands($store_id)
    {
        try {
            $brand_ids = Item::query()
                ->where('store_id', $store_id)
                ->active()
                ->pluck('brand_id');
            $brands = Brand::query()
                ->whereIn('id', $brand_ids)
                ->get();

            return response()->json(Helpers::brand_data_formatting($brands, true), 200);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e], 403);
        }
    }


    public function get_items($brand_id)
    {
        try {
            $items = Item::when(request('store_id'), function ($q) {
                $q->where('store_id', request('store_id'));
            })->whereHas('brand', function ($brand) use ($brand_id) {
                $brand->where('id', $brand_id);
            })->get();
            $items = Helpers::product_data_formatting($items, true, false, app()->getLocale());
            return response()->json($items, 200);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e], 403);
        }
    }
}
