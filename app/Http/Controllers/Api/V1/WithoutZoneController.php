<?php

namespace App\Http\Controllers\Api\V1;


use App\CentralLogics\Helpers;
use App\CentralLogics\StoreLogic;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Store;
use Illuminate\Http\Request;

class WithoutZoneController extends Controller
{
    public function get_stores(Request $request)
    {
        try {
            $limit = $request->limit ?? 10;
            $stores = Store::query()
                ->withOpenStatus()
                ->with('module', function ($query) {
                    $query->active();
                })
                ->active()
                ->orderBy('open', 'desc')
                ->paginate($limit);

            return response()->json($stores);
        } catch (\Exception $e) {
            return response()->json([], 200);
        }
    }

    public function get_items()
    {
        $limit = $request->limit ?? 10;
        $items = Item::query()
            ->active()
            ->with([
                'module' => function ($query) {
                    return $query->active();
                }
            ])
            ->paginate($limit);

        return response()->json($items);
    }
}
