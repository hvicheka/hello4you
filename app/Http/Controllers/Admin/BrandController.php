<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\CentralLogics\Helpers;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\DB;
use App\Models\Translation;

class BrandController extends Controller
{
    function index(Request $request)
    {
        $brands=Brand::with('module')
        ->when($request->query('module_id', null), function($query)use($request){
            return $query->module($request->query('module_id'));
        })
        ->latest()->paginate(config('default_pagination'));
        return view('admin-views.brand.index',compact('brands'));
    }

    function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
        ], [
            'name.required' => trans('messages.Name is required!'),
        ]);

        $brand = new Brand();
        $brand->name = $request->name[array_search('en', $request->lang)];
        $brand->image = Helpers::upload('brand/', 'png', $request->file('image'));
        $brand->module_id = $request->module_id;
        $brand->save();

        $data = [];
        foreach($request->lang as $index=>$key)
        {
            if($request->name[$index] && $key != 'en')
            {
                array_push($data, Array(
                    'translationable_type'  => 'App\Models\Brand',
                    'translationable_id'    => $brand->id,
                    'locale'                => $key,
                    'key'                   => 'name',
                    'value'                 => $request->name[$index],
                ));
            }
        }
        if(count($data))
        {
            Translation::insert($data);
        }

        Toastr::success(trans('messages.brand_added_successfully'));
        return back();
    }

    public function edit($id)
    {
        $brand = Brand::withoutGlobalScope('translate')->findOrFail($id);
        return view('admin-views.brand.edit', compact('brand'));
    }

    public function status(Request $request)
    {
        $brand = Brand::find($request->id);
        $brand->status = $request->status;
        $brand->save();
        Toastr::success(trans('messages.brand_status_updated'));
        return back();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:100',
        ]);
        $brand = Brand::find($id);

        $brand->name = $request->name[array_search('en', $request->lang)];
        $brand->image = $request->has('image') ? Helpers::update('brand/', $brand->image, 'png', $request->file('image')) : $brand->image;
        $brand->save();
        foreach($request->lang as $index=>$key)
        {
            if($request->name[$index] && $key != 'en')
            {
                Translation::updateOrInsert(
                    ['translationable_type'  => 'App\Models\Brand',
                        'translationable_id'    => $brand->id,
                        'locale'                => $key,
                        'key'                   => 'name'],
                    ['value'                 => $request->name[$index]]
                );
            }
        }
        Toastr::success(trans('messages.brand_updated_successfully'));
        return back();
    }

    public function delete(Request $request)
    {
        $brand = Brand::findOrFail($request->id);
        if ($brand){
            $brand->delete();
            Toastr::success('Brand removed!');
        }
        return back();
    }

    public function get_all(Request $request){
        $data = Brand::where('name', 'like', '%'.$request->q.'%')->limit(8)->get();
        if(isset($request->all))
        {
            $data[]=(object)['id'=>'all', 'text'=>'All'];
        }
        return response()->json($data);
    }

    public function update_priority(Brand $brand, Request $request)
    {
        $priority = $request->priority??0;
        $brand->priority = $priority;
        $brand->save();
        Toastr::success(trans('messages.brand_priority_updated successfully'));
        return back();

    }

    public function bulk_import_index()
    {
        return view('admin-views.brand.bulk-import');
    }

    public function bulk_import_data(Request $request)
    {
        try {
            $collections = (new FastExcel)->import($request->file('products_file'));
        } catch (\Exception $exception) {
            Toastr::error(trans('messages.you_have_uploaded_a_wrong_format_file'));
            return back();
        }

        $data = [];
        foreach ($collections as $collection) {
            if ($collection['name'] === "" || !is_numeric($collection['module_id'])) {

                Toastr::error(trans('messages.please_fill_all_required_fields'));
                return back();
            }

            array_push($data, [
                'name' => $collection['name'],
                'image' => $collection['image'],
                'module_id' => $collection['module_id'],
                'priority'=>is_numeric($collection['priority']) ? $collection['priority']:0,
                'status' => empty($collection['status'])?1:$collection['status'],
                'created_at'=>now(),
                'updated_at'=>now()
            ]);
        }
        DB::table('brands')->insert($data);
        Toastr::success(trans('messages.brand_imported_successfully', ['count'=>count($data)]));
        return back();
    }

    public function bulk_export_index()
    {
        return view('admin-views.brand.bulk-export');
    }

    public function bulk_export_data(Request $request)
    {
        $request->validate([
            'type'=>'required',
            'start_id'=>'required_if:type,id_wise',
            'end_id'=>'required_if:type,id_wise',
            'from_date'=>'required_if:type,date_wise',
            'to_date'=>'required_if:type,date_wise'
        ]);
        $brands = Brand::when($request['type']=='date_wise', function($query)use($request){
            $query->whereBetween('created_at', [$request['from_date'].' 00:00:00', $request['to_date'].' 23:59:59']);
        })
        ->when($request['type']=='id_wise', function($query)use($request){
            $query->whereBetween('id', [$request['start_id'], $request['end_id']]);
        })
        ->get();
        return (new FastExcel($brands))->download('Brands.xlsx');
    }

    public function search(Request $request){
        $key = explode(' ', $request['search']);
        $brands=Brand::
       where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->limit(50)->get();


        return response()->json([
            'view'=>view('admin-views.brand.partials._table',compact('brands'))->render(),
            'count'=>$brands->count()
        ]);
    }
}
