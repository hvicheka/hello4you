<?php

namespace App\Http\Controllers\Admin;

use App\CentralLogics\Helpers;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class TestingController extends Controller
{

    public function send_global_notification(Request $request)
    {

        $notification = Notification::query()->inRandomOrder()->first();

        $topic_all_zone = [
            'customer' => 'all_zone_customer',
            'deliveryman' => 'all_zone_delivery_man',
            'store' => 'all_zone_store',
        ];

        $topic_zone_wise = [
            'customer' => 'zone_' . $request->zone . '_customer',
            'deliveryman' => 'zone_' . $request->zone . '_delivery_man',
            'store' => 'zone_' . $request->zone . '_store',
        ];

        $topic = $request->zone == 'all' ? $topic_all_zone[$request->target] : $topic_zone_wise[$request->target];

        if ($notification->image) {
            $notification->image = url('/') . '/storage/app/public/notification/' . $notification->image;
        }

        try {
            $result = Helpers::send_push_notif_to_topic($notification, $topic, 'general');
        } catch (\Exception $e) {
            Toastr::warning(trans('messages.push_notification_faild'));
        }

        return $result;

    }

}
