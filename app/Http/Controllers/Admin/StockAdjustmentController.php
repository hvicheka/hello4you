<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Item;
use App\Models\StockAdjustment;
use App\Models\StockAdjustmentProduct;
use App\Scopes\StoreScope;
use Illuminate\Http\Request;

class StockAdjustmentController extends Controller
{
    public function index()
    {
        $stocks = StockAdjustment::query()
            ->latest()
            ->when(\request()->input('store_id'), function ($query) {
                return $query->where('store_id', \request()->input('store_id'));
            })
            ->paginate(config('default_pagination'));
        return view('admin-views.stock-adjustment.index', compact('stocks'));
    }

    public function create()
    {
        return view('admin-views.stock-adjustment.create');
    }

    public function store(Request $request)
    {
        $stock_adjustment_product = [];

        $stock_adjustment = StockAdjustment::create([
            'store_id' => $request->store_id,
            'ref_no' => $request->ref_no,
            'total' => $request->final_total,
            'date' => $request->date,
            'reason' => $request->reason,
            'userable_id' => auth('admin')->id(),
            'userable_type' => Admin::class
        ]);

        foreach ($request->products as $product) {
            $stock_adjustment_product[] = $product;
        }

        foreach ($stock_adjustment_product as $product) {
            $item = Item::query()->find($product['product_id']);
            if ($item) {
                StockAdjustmentProduct::create([
                    'adjust_type' => "stock_adjust",
                    'previous_quantity' => $item->stock ?? 0,
                    'stock_adjustment_id' => $stock_adjustment->id,
                    'item_id' => $product['product_id'],
                    'quantity' => $product['quantity'],
                    'unit_price' => $product['unit_price'],
                    'price' => $product['price']
                ]);
                $item->decrement('stock', $product['quantity']);
            }
        }

        toastr(_translate('Stock adjustment added successfully'));
        return redirect(url('/admin/stock-adjustment'));
    }

    public function show($id)
    {
        $stock = StockAdjustment::query()->findOrFail($id);
        $stock_products = StockAdjustmentProduct::query()
            ->where('stock_adjustment_id', $id)
            ->where( 'adjust_type',"stock_adjust")
            ->get();
        return view('admin-views.stock-adjustment.show', compact('stock', 'stock_products'));
    }

    public function get_products(Request $request)
    {
        if (!$request->store_id || $request->store_id == 0) {
            return response()->json(null);
        }
        $items = Item::withoutGlobalScope(StoreScope::class)
            ->select('id', 'name')
            ->when($request->store_id, function ($q) use ($request) {
                $q->where('store_id', $request->store_id);
            })->when($request->q, function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->q . '%');
            })
            ->when($request->module_id, function ($q) use ($request) {
                $q->where('module_id', $request->module_id);
            })
            ->get();
        $results = [];
        foreach ($items as $item) {
            $results[] = [
                'id' => $item->id,
                'text' => $item->name
            ];
        }
        return response()->json($results);
    }

    public function get_products_detail(Request $request)
    {
        $item = Item::query()
            ->select('id', 'name', 'price')
            ->where('id', $request->item_id)
            ->first();
        return response()->json($item);
    }


    public function destroy($id)
    {
        $stock = StockAdjustment::query()->findOrFail($id);
        $stock_products = StockAdjustmentProduct::query()
            ->where('stock_adjustment_id', $stock->id)
            ->where( 'adjust_type',"stock_adjust")
            ->get();
        $product_stock_ids = [];
        foreach ($stock_products as $product) {
            $item = Item::find($product->item_id);
            $item->increment('stock', $product->quantity);
            $product_stock_ids[] = $product->id;
        }

        StockAdjustmentProduct::destroy($product_stock_ids);
        $stock->delete();
        return redirect(url('/admin/stock-adjustment'));
    }
}
