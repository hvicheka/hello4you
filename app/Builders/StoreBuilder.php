<?php

namespace App\Builders;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class StoreBuilder extends Builder
{
    public function withOpenStatus()
    {
        $timezone = config('app.kh_timezone');
        $now = \Carbon\Carbon::now()->setTimezone($timezone)->format('H:i:s');
        return $this->addSelect(DB::raw('*, IF(((select count(*) from `store_schedule` where `stores`.`id` = `store_schedule`.`store_id` and `store_schedule`.`day` = ' . now()->dayOfWeek . ' and `store_schedule`.`opening_time` < "' . $now . '" and `store_schedule`.`closing_time` >"' . $now . '") > 0), true, false) as open'));
    }

    public function active()
    {
        return $this->where('status', 1);
    }

    public function search()
    {
        collect(str_getcsv(request()->input('q'), ' ', '"'))->filter()->each(function ($term) {
            $term = "%$term%";
            return $this->where(function ($query) use ($term) {
                return $query->where('name', 'like', $term);
            });
        });
    }

}
