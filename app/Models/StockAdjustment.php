<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StockAdjustment extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $table = 'stock_adjustment';

    protected $casts = [
        'date' => 'datetime',
    ];

    public function userable()
    {
        return $this->morphTo();
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

}
