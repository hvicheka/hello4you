<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'f_name',
        'l_name',
        'phone',
        'email',
        'password',
        'login_medium',
        'social_id',
        'country_code',
        'national_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'interest',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_phone_verified' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'order_count' => 'integer'
    ];


    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function addresses()
    {
        return $this->hasMany(CustomerAddress::class);
    }

    // /**
    //  * Get the user's order count.
    //  *
    //  * @return integer
    //  */
    // public function getOrderCountAttribute()
    // {
    //     return $this->orders->count();
    // }

    public function points()
    {
        return $this->hasMany(Point::class, 'customer_id', 'id');
    }

    public function scopeWithShares($query)
    {
        $query->withCount([
            'points AS total_points' => function ($query) {
                $query->select(DB::raw("SUM(points) as points_sum"));
            },
            'points AS point_to_usd' => function ($query) {
                $query->select(DB::raw("SUM(amount) as share_sum"));
            },
        ])
            ->withCasts([
                'total_points' => 'float',
                'total_shares' => 'float'
            ]);
    }

}
