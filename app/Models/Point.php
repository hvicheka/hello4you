<?php

namespace App\Models;

use App\CentralLogics\Helpers;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{

    protected $table = 'points';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

}
