<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StockAdjustmentProduct extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $table = 'stock_adjustment_products';

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function userable()
    {
        return $this->morphTo();
    }

}
