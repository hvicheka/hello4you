<?php

if (!function_exists('default_lang')) {
    function default_lang()
    {
        // return session()->get('local') ?? app()->getLocale();
        return 'en';
    }
}


if (!function_exists('remove_invalid_character')) {
    function remove_invalid_character($str)
    {
        return str_ireplace(['\'', '"', ',', ';', '<', '>', '?'], ' ', $str);
    }
}

if (!function_exists('_translate')) {

    function _translate($key)
    {
        $local = default_lang();
        app()->setLocale($local);

        $lang_array = include(base_path('resources/lang/' . $local . '/messages.php'));
        $processed_key = ucfirst(str_replace('_', ' ', remove_invalid_character($key)));

        if (!array_key_exists($key, $lang_array)) {
            $lang_array[$key] = $processed_key;
            $str = "<?php return " . var_export($lang_array, true) . ";";
            file_put_contents(base_path('resources/lang/' . $local . '/messages.php'), $str);
            $result = $processed_key;
        } else {
            $result = __('messages.' . $key);
        }
        return $result;
    }

}


if (!function_exists('format_phone')) {
    function format_phone($phone)
    {
        $result = [
            'country_code' => null,
            'national_number' => null
        ];
        try {
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            $parse = $phoneUtil->parse($phone);
            $country_code = $parse->getCountryCode();
            $national_number = $parse->getNationalNumber();
            $phone = "+" . $country_code . $national_number;
            $result['country_code'] = $country_code;
            $result['national_number'] = $national_number;
            $result['phone'] = $phone;
        } catch (\libphonenumber\NumberParseException $e) {
            $result['phone'] = $phone;
        }
        return $result;
    }
}


if (!function_exists('validate_phone')) {

    function validate_phone($phone)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        try {
            $parse = $phoneUtil->parse($phone);
            $country_code = $parse->getCountryCode();
            $phoneNumber = $parse->getNationalNumber();
            return "+" . $country_code . $phoneNumber;
        } catch (\libphonenumber\NumberParseException $e) {
            return $phone;
        }
    }
}
